#include <Entropy.h>
#include <EEPROM.h>
/* Récepteur 433 MHz */
#include <VirtualWire.h>
#include <Ethernet.h>    /* Pour la shield ethernet */
#include <SPI.h>         /* Pour la communication bas niveau (carte SD et ethernet) */


String Uuid;
String eeprom0;
String eeprom1;
String eeprom2;
String eeprom3; 
char Message[VW_MAX_MESSAGE_LEN]; 

/** Taille du buffer pour la lecture du fichier texte et l'envoi de la requete HTTP */
static const byte BUFFER_SIZE = 100;
/** Adresse MAC de l'ethernet shield */
static byte localMacAddress[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0x6A };
/** Adresse IP serveur perso PHP */
static IPAddress serverIpAddress(192, 168, 42, 181);
char URLAddress[]="iot.exemple.fr";


String GetID() {
//Read the EEPROM to get the ID
  eeprom0 = EEPROM.read(0);
  eeprom1 = EEPROM.read(1);
  eeprom2 = EEPROM.read(2);
  eeprom3 = EEPROM.read(3);

//Making the ID
String Uuid = String(eeprom0 + "-" + eeprom1 + "-" + eeprom2 + eeprom3);
return Uuid;
}


void setup()
{
 
   Serial.begin(9600);
   vw_set_rx_pin(7);
   vw_setup(2000); // initialisation de la communication à 2000 b/s
   vw_rx_start();  // Activation de la réception

 // Start ID section
Uuid = GetID();
  Serial.print("Actual ID : "); 
  Serial.print(Uuid);
  Serial.println();
    
if (Uuid == "0-0-00") {
     
GeneratetID();
 Uuid = GetID();
  Serial.println();
  Serial.print("ID : "); 
  Serial.print(Uuid);
  }

// Stop ID section

//ETH
    // start Ethernet and UDP
  if (Ethernet.begin(localMacAddress) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // no point in carrying on, so do nothing forevermore:
    for(;;)
      ;
  }


printIPAddress();
//Fin ETH

Serial.println("");
Serial.println("Waiting for messages");
}
 
void loop()
{
  // array who contain the message (max size VW_MAX_MESSAGE_LEN)
uint8_t buf[VW_MAX_MESSAGE_LEN];
uint8_t buflen = VW_MAX_MESSAGE_LEN; // The max size of the array

   if (vw_have_message) // If a message is receive
   {
   
     
if (vw_get_message(buf, &buflen)) // get the message
  {
     int i;

    for (i = 0; i < buflen; i++)
    {               
     Message[i] = char(buf[i]);
    }

   Message[buflen] = '\0';

if (sendToAPI(Message, Uuid)) { /* Réussite */
      /* Message de debug */
      Serial.println(F(""));
    Serial.println(F("Envoi ok"));
  /* Tentative d'envoi des rapports en attente */
   } else { /* Echec */
  /* Message de debug */
    Serial.println(F("Echec envoi"));
                      }

   }
}
}




String GeneratetID() {
Serial.print("ID GENERATION");    
 
  Entropy.Initialize();  

  uint32_t random_id;
  random_id = Entropy.random();

//Setting ID into the EEPROM
EEPROM.put(0, random_id);

 //Read the EEPROM to get the ID
  eeprom0 = EEPROM.read(0);
  eeprom1 = EEPROM.read(1);
  eeprom2 = EEPROM.read(2);
  eeprom3 = EEPROM.read(3);

//Making the ID
Uuid = String(eeprom0 + "-" + eeprom1 + "-" + eeprom2 + eeprom3);
         Serial.println("");
  Serial.print("The generated UUID number sting is ");
  Serial.print(Uuid);
         Serial.println("");
return Uuid;
}




//static void sendToAPI (char* c, String Uuid) {
boolean sendToAPI (char* c,String Uuid) {
  Serial.println("Sending message...");

 /* Buffer contenant une partie de la requete HTTP */
  char buffer[BUFFER_SIZE];
  
  /* Ouverture de la connexion TCP */
  EthernetClient client;
  delay(1000);
  Serial.print(F("Connexion au serveur ... "));
  if (client.connect(URLAddress, 80)) {
  
  Serial.println(F("Ok !"));
  /* Envoi de la requete HTTP */
  client.print(F("GET /probe.php?uuid="));
  client.print(Uuid);
  client.print("&");
  client.print(c);
  client.println(F(" HTTP/1.1"));
  client.println("Host: iot.exemple.fr");
  client.println("Connection: close");
  client.println();

} else {
    // if you didn't get a connection to the server:
    Serial.println("connection failed");
  }
  
  /* Fin de la requete HTTP */
  Serial.println(F("Fin envoi ..."));
  delay(1000); /* Permet au serveur distant de recevoir les données avant de fermer la connexion */
 client.stop();
   Serial.print(URLAddress);
   Serial.print("/probe.php?uuid=");
   Serial.print(Uuid);
   Serial.print("&");
   Serial.print(c);
   
  /* Pas d'erreur */
  return true;
 Serial.println("");
  Serial.println("Message Send...");
}

void printIPAddress()
{
  Serial.print("My IP address: ");
  for (byte thisByte = 0; thisByte < 4; thisByte++) {
    // print the value of each byte of the IP address:
    Serial.print(Ethernet.localIP()[thisByte], DEC);
    Serial.print(".");
  }

  Serial.println();
}